/*
 * Globalcode - "The Developers Company"
 *
 * Academia do Java
 *
 */
package br.com.globalcode.aj3.teste;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import br.com.globalcode.aj3.beans.Cliente;

public class TesteList {

	final Cliente c1 = new Cliente("Cliente1", "1111", "cpf1");
	final Cliente c2 = new Cliente("Cliente2", "2222", "cpf2");
	final Cliente c3 = new Cliente("Cliente3", "3333", "cpf3");
	final Cliente c4 = new Cliente("Cliente3", "3333", "cpf3");
	final List clientes = new ArrayList();
	{
		this.clientes.add(this.c1);
		this.clientes.add(this.c2);
		this.clientes.add(this.c3);
		this.clientes.add(this.c4);
	}

	@Test
	public void deveEncontrar4Nomes() {
		final List<String> nomesEncontrados = new ArrayList<>();
		for (int posicao = 0; posicao < this.clientes.size(); posicao++) {
			final Object clienteComoObjeto = this.clientes.get(posicao);
			if (clienteComoObjeto instanceof Cliente) {
				final Cliente cliente = (Cliente) clienteComoObjeto;
				nomesEncontrados.add(cliente.getNome());
			}
		}
		final List<String> nomesEsperados = Arrays.asList("Cliente1", "Cliente2", "Cliente3", "Cliente3");
		Assert.assertEquals(nomesEsperados, nomesEncontrados);
	}

	@Test
	public void deveFazerOutraCoisa() {
		// 1. Imprima somente o nome de cada cliente da lista de clientes
		for (int posicao = 0; posicao < this.clientes.size(); posicao++) {
			final Object clienteComoObjeto = this.clientes.get(posicao);
			if (clienteComoObjeto instanceof Cliente) {
				final Cliente cliente = (Cliente) clienteComoObjeto;
				System.out.println(cliente.getNome());
			}
		}
		// alternativa 2
		for (final Object clienteComoObjeto : this.clientes) {
			if (clienteComoObjeto instanceof Cliente) {
				final Cliente cliente = (Cliente) clienteComoObjeto;
				System.out.println(cliente.getNome());
			}
		}
		// alternativa 3

		final List<Cliente> clientes2 = new ArrayList<>();
		clientes2.add(this.c1);
		clientes2.add(this.c2);
		clientes2.add(this.c3);
		clientes2.add(this.c4);

		for (final Cliente cliente : clientes2) {
			System.out.println(cliente.getNome());
		}

		// alternativa 4

		clientes2.stream().map(cliente -> cliente.getNome()).forEach(System.out::println);
	}

}